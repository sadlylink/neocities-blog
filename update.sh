#!/bin/bash

# Error handeling
set -e

# Function to download a file from a URL to a local folder
download_file() {
    local url=$1
    local local_folder=$2
    local file_name=$(basename "$url")

    # Create the local folder if it doesn't exist
    mkdir -p "$local_folder"

    # Download the file
    if curl -o "$local_folder/$file_name" "$url"; then
        echo "Downloaded $file_name to $local_folder"
    else
        echo "Failed to download $file_name from $url"
        exit 1
    fi
}

# Function to run the Python script
run_python_script() {
    local script_path=$1

    if python3 "$script_path"; then
        echo "Successfully ran $script_path"
    else
        echo "Failed to run $script_path"
        exit 1
    fi
}

# Define URLs and local folders
declare -A files_to_download=(
    ["https://nuthead.neocities.org/ring/ring.js"]="./assets/js"
    ["https://unpkg.com/interactjs/dist/interact.min.js"]="./assets/js"
)

# Define Python script path
python_script_path="_data/quotes.py"

# Prompt the user to select an action
echo "Select an action:"
options=("Download ring.js" "Download interact.min.js" "Run quotes.py")
select opt in "${options[@]}"; do
    case $opt in
        "Download ring.js")
            download_file "https://nuthead.neocities.org/ring/ring.js" "./assets/js"
            break
            ;;
        "Download interact.min.js")
            download_file "https://unpkg.com/interactjs/dist/interact.min.js" "./assets/js"
            break
            ;;
        "Run quotes.py")
            run_python_script "$python_script_path"
            break
            ;;
        *)
            echo "Invalid selection. Please try again."
            ;;
    esac
done
