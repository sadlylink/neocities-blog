# Contributor Covenant Code of Conduct

## Purpose

The purpose of this document is to articulate my personal values and principles as the sole owner and contributor of this blog. It serves as a guideline for behavior and interaction, ensuring a respectful and inclusive environment for all who engage with this space.

## Pledge

In the interest of fostering an open and welcoming environment, I pledge to make participation in this blog a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

## Standards of Behavior

### Positive Contributions

Examples of behavior that contribute to creating a positive environment include:

- Using welcoming and inclusive language.
- Being respectful of differing viewpoints and experiences.
- Gracefully accepting constructive criticism and feedback.
- Focusing on what is best for the community and the blog's mission.
- Showing empathy and kindness towards others.

### Unacceptable Behavior

Examples of unacceptable behavior include:

- The use of sexualized language or imagery, and unwelcome sexual attention or advances.
- Trolling, insulting or derogatory comments, and personal or political attacks.
- Public or private harassment.
- Publishing others' private information without explicit permission.
- Other conduct which could reasonably be considered inappropriate in a professional or public setting.

## Responsibilities

As the sole maintainer of this blog, I am responsible for clarifying the standards of acceptable behavior and will take appropriate and fair corrective action in response to any instances of unacceptable behavior. I reserve the right to remove, edit, or reject comments, posts, and other contributions that do not align with this Code of Conduct. I may also temporarily or permanently ban any contributor or commentor for behaviors that I deem inappropriate, threatening, offensive, or harmful.

## Scope

This Code of Conduct applies within all blog spaces, including comments, social media interactions, and any public representation of the blog. Examples of representing the blog include using an official blog email address or social media accounts.

## Reporting and Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported to me at [info@sadly.link](mailto:info@sadly.link). All complaints will be reviewed and investigated, and I will respond in a manner that is deemed necessary and appropriate to the circumstances. I am committed to maintaining confidentiality regarding the reporter of an incident.

This document can also be referenced to hold me accountable for any breaches of this Code of Conduct.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at <https://www.contributor-covenant.org/version/1/4/code-of-conduct.html>.

[homepage]: <https://www.contributor-covenant.org>

For answers to common questions about this Code of Conduct, see <https://www.contributor-covenant.org/faq>.