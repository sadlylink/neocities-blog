#!/bin/bash

# Error handeling
set -e

echo "Building Jekyll site..."
bundle exec jekyll build
echo "Jekyll site built successfully."

echo "Signing About Page"
~/./kryptor -sy Ed//HH3ru7EjBZcKBxaHicMqYNtYRxYxkdcXVaTNnFT5xPU= _site/about/index.html
~/./kryptor -sy Ed//HH3ru7EjBZcKBxaHicMqYNtYRxYxkdcXVaTNnFT5xPU= _site/about/verify.html
echo "About Page signed successfully."
