---
layout: blog
title: Blog
date: 2024-08-16
keywords: learning, writing, philosophy, computer science
description: Blog about coding, philosophy, writing and other things. 
    So basically life, the universe and everything...
permalink: /blog/
cat: blog
---

# My Blog

In this Blog, I post stuff that took me longer to write. <br>
A more personal "blog"/logbook can be found on the [miscellaneous page](/misc/).

<div class="retro-disclaimer">
  <strong>Or checkout <a href="https://listed.to/@sadlyLink">listed.to/@sadlyLink</a> for a more modern looking blog layout where I will be posting my blog posts</strong>
</div>