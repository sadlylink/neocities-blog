---
permalink: /blog/tutorial.cpp
---
#include <iostream>
#include <string>
#include <algorithm> // for std::transform
#include <cctype>    // for std::tolower

int main() {
    // Declare variables
    bool rightAnswer = false;
    std::string answer;

    // While the answer is not a correct one
    while (!rightAnswer) {
      // Prompt the user and get input
        std::cout << "Hello there!" << std::endl;
        std::getline(std::cin, answer);
        
        // Convert the user's input to lowercase for case-insensitive comparison
        std::transform(answer.begin(), answer.end(), answer.begin(), 
        [](unsigned char c) { return std::tolower(c); });

       // Check if the answer matches predefined correct responses
        if (answer == "general kenobi") {
            // Display message for correct response
            std::cout << "You are a bold one *coughs in cyber*" << std::endl;
            
            // Set the flag to exit the while loop
            rightAnswer = true;
        }
        else if (answer == "beebop") {
          // Display message for another correct response
            std::cout << "Come closer, little friend" << std::endl;
            // If it is a right answer set variable to true so the while loop ends
            rightAnswer = true;
        } else{
          // We do nothing otherwise
          // You wouldn't need an 'else' here if you want to ignore other inputs
        }
    }
    return 0; // The End
}