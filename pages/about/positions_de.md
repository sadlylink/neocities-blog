---
layout: post
title: Positionen
date: 2023-10-04
description: Meine Position zu verschiedenen Themen und dem Wesen offener Diskussionen, persönliche Freiheiten, Moral, Ethik und Menschenrechte in der heutigen Gesellschaft.
keywords: philosophie, positionen, persönlich, personal website, menschenrechte, LGBTQIA+, freiheit, diskussion, debatte, deutsch
permalink: /about/positions_de.html
last-edited: 2024-08-12
language: de
comments: false
---
<span id="language" > <a href="/about/positions.html"><svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate" viewBox="0 0 16 16"><path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286zm1.634-.736L5.5 3.956h-.049l-.679 2.022z"/><path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zm7.138 9.995q.289.451.63.846c-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6 6 0 0 1-.415-.492 2 2 0 0 1-.94.31"/>
</svg> en</a></span>
<div class="retro-disclaimer">
<strong>Das Folgende ist nur meine Meinung. Diskussionen sind immer erwünscht und geschätzt!</strong>
</div>

## Offene Diskussion

In der heutigen Gesellschaft ist es entscheidend, offene Diskussionen über wichtige Themen zu führen. Echter Fortschritt entsteht durch vernünftige, aufgeschlossene und zielorientierte Debatten.
Im Sinne von Habermas trifft **»der eigentümlich zwanglose Zwang des besseren Arguments«** zu. Wenn jemand einen besseren Standpunkt vorbringt, sollte dies geschätzt werden. Das Ziel sollte sein, die Positionen zu stärken. Das bedeutet, die besten möglichen Argumente für *alle* Seiten zu verbessern und zu formulieren und dann durch Abwägen der Argumente das Beste auszuwählen. In jedem Fall handelt es sich oft um ein utopisches Ideal. Dennoch glaube ich, dass wir durch die verstärkte Anwendung dieser Methode in unseren Diskussionen einen Schritt nach vorne machen könnten.

--> [Post zu Debatenkultur](/blog/debate-culture.html)

## Persönliche Freiheiten

Persönliche Freiheiten sind ein weiterer wesentlicher Aspekt unserer Gesellschaft. Jeder sollte in der Lage sein, seine Meinungen offen und authentisch auszudrücken.
Menschen sollten frei sein, sich selbst auszudrücken und so zu sein, wie sie sind, dabei aber die Freiheiten anderer respektieren.

> Die Freiheit des Einzelnen endet dort, wo die Freiheit des Anderen beginnt

Die Grundlage unserer Gesellschaften ist die Maximierung der sozialen Freiheiten durch die Aufgabe einiger individueller Freiheiten an den Staat.
Dies ähnelt dem Prozess des Entstehens, wenn eine Gruppe durch die Interaktion ihrer Elemente eine neue Eigenschaft erwirbt. 
Wenn eine Gesellschaft den Gesamtnutzen für ihre Bürger verliert, kann diese Gesellschaft nicht funktionieren oder zumindest nicht lange überleben. Daher sollten meiner Meinung nach die Freiheiten eines Individuums in jeder Gesellschaft auf demokratischen Werten und Menschenrechten beruhen.

## Moral und Ethik

Dies bringt mich zur Rechtfertigung des Rahmens für die Freiheiten einer Person.

Als Atheist denke ich, dass moralische Werte nicht transzendent oder von Gott gegeben sind. Als soziale Wesen teilen wir jedoch ein gemeinsames Verständnis von richtig und falsch, das in gewissem Maße von unserer Umgebung beeinflusst wird.

Sowohl die Vorstellung von einem imaginären allmächtigen Wesen als auch die Idee, sich ausschließlich auf den höchsten Nutzen zu konzentrieren, begehen meines Erachtens, einen Fehler, da sie die Moral in ihrem Kern entfremden. Wenn Gott Ihnen sagt, Ihr Kind zu töten, müssen Sie Ihr Kind daher töten, obwohl dies gegen unsere Natur geht. 
Gleiches gilt für den Utilitarismus. Wenn es die Gesellschaftsnützlichkeit in enormem Maße maximiert, ein Kind zu töten, dann los geht's.
Natürlich handelt es sich hierbei um Übertreibungen. Gerade beim Glauben gibt es sehr viele Aspekte, die mit einbezogen werden müssen, was hier wichtig ist. 

Aber ich denke, sie zeigen, dass Moral ein komplexes Konstrukt ist, das nicht einfach auf eine Erfindung der Vorstellung oder einige Matrizen zurückgeführt werden kann.

Moral scheint vielmehr eine psychologische und evolutionäre Eigenschaft zu sein. Diejenigen, die altruistisch handeln, profitieren in der Regel davon. Schließlich sind Menschen soziale Wesen und haben aufgrund dieser Tatsache überlebt.
Zum Beispiel liegt es in unserer Natur, andere Menschen nicht zu töten, und wir können sehen, dass solche Werte viele Veränderungen in verschiedenen Gesellschaften unterstützt haben.

## Privatsphäre und Datenschutz

Privatsphäre ist eine sehr subjektive Angelegenheit. Subjektiv in der Hinsicht, dass man unterschiedliche Schmerzgrenzen hat, wie viel über einen online zur Verfügung steht, wie sehr man sich um den Schutz der eigenen Daten bemüht und wie nötig ein guter Schutz der eigenen Daten ist.

Meiner Meinung nach bedeutet Privatsphäre die Möglichkeit, die eigenen Daten zu kontrollieren.
Die Kontrolle über die eigenen Daten bedeutet die Kontrolle über das eigene Leben. Heutzutage basiert unser tägliches Leben auf den Daten, die über uns verfügbar sind.

Versicherungsbeiträge nutzen Informationen, die Google, Facebook, Autos und andere über einen sammeln. Was man online posten, könnte Einfluss darauf haben, welchen Job man bekommen kann, oder man könnten von der eigenen Regierung verfolgt werden. 
Selbst diejenigen, die die Möglichkeit eines demokratischen Staates haben, sollten das bedenken. Was heute legal ist, kann morgen schon nicht mehr legal sein. Regime kommen und gehen, aber die eigenen Daten bleiben erhalten. 
Jemand, wie Trump sagt, er wäre ein Diktator für "einen Tag", aber wer sagt denn, dass man in Zukunft nicht in einer autoritären Gesellschaft leben wird?

Privatsphäre ermöglicht persönliche Freiheiten. Sie ist ein wesentliches Recht und ohne sie sind unsere persönlichen Freiheiten, die freie Diskussion und die Demokratie in Gefahr.

--> [Post zu Privatsphäre](/blog/my-privacy-journey.html)

## Menschenrechte

Schließlich sollten Menschenrechte die Grundlage für das Handeln der Gesellschaft und der Staaten sein. Es ist entscheidend  Zivilcourage zu zeigen. Wir müssen Kinder schützen, Bildung ermöglichen, die Todesstrafe verhindern und die Rechte der LGBTQIA+ Community bewahren und durchsetzen. 
Zu alle dem gehört auch das Handeln gegen den Klimawandel und demokratiefeindliche Ideologien wie den Faschismus.

Einige, wenn nicht alle diese Punkte werde ich in zukünftigen Blogbeiträgen näher behandeln. Haben Sie eine andere Meinung zu einem dieser Punkte? Oder weitere Ideen? Lassen Sie es mich bitte wissen.
