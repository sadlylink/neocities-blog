---
layout: post
title: Positions
date: 2023-08-02
description: My take on different topics such as open discussions, personal freedoms, morals, ethics, and human rights in today's society.
keywords: Philosophy, Learn, Human Rights, LGBTQIA+, Freedom, Discussion, Debate
permalink: /about/positions.html
last-edited: 2024-08-12
language: en
comments: false
---

<span id="language" > <a href="/about/positions_de.html"><svg xmlns="https://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate" viewBox="0 0 16 16"><path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286zm1.634-.736L5.5 3.956h-.049l-.679 2.022z"/><path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zm7.138 9.995q.289.451.63.846c-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6 6 0 0 1-.415-.492 2 2 0 0 1-.94.31"/>
</svg> de</a></span>
<div class="retro-disclaimer">
  <strong>The following is my opinion. Discussion is always wanted and appreciated!</strong>
</div>

## Open Discussion

In today's society, it is crucial to have open discussions about important topics. Real progress comes from reasonable, open-minded, and goal-oriented discussions. In the spirit of Habermas, **“The Unforced Force of the better Argument”** applies. If someone makes a better point, it should be appreciated. 
The aim should be to steelmanning the positions. That is, to improve and formulate the best possible arguments for *all* sides, and then, by weighing the arguments, to choose the best one. 
In any case, this is often a utopian ideal. Nonetheless, I believe that by integrating this method more into our discussions, we could leap forward.

--> [Post on Debate Culture](/blog/debate-culture.html)

## Personal Freedoms

Personal freedoms are another essential aspect of our society. Everyone should be able to express their opinions openly and authentically. People should be free to express themselves and be who they are, while respecting the freedoms of others.

> One persons freedom ends where anothers begins.

The basis of our societies is the maximisation of social freedoms by surrendering some individual freedoms to the state. This is similar to the process of emergence when a group acquires a new property through the interaction of its elements.
If a society loses the overall benefit to its citizens, that society cannot function, or at least cannot sustain itself for long.
So to put this together, I think the freedoms of an individual in any society should be based on democratic values and human rights.

## Morals and Ethics

This brings me to the justification for the frame of a person's freedoms.

As an atheist, I think that moral values are not transcendent or god-given. However, as social beings, we share a common understanding of right and wrong, which is, to some extent influenced by our environment.

Both the idea that some imaginative omnipotent being and the idea to just focus on the highest benefit, commit a fallacy (IMO) because they alienate morality in its core. If god tells you to kill your child, you therefore must kill your child, even though this goes against our very nature. The same is true for Utilitarianism. If it maximises society’s utility to kill a child to an enormous amount, then go for it.

Of course, these are exaggerations. Especially when it comes to beliefs, there are many aspects that need to be taken into account, which is important here. 

But I think these examples show that morality is a complex construct that cannot simply be imposed on a figment of the imagination or a few matrices.

Morality seems to be more of a psychological and evolutionary trait. Those who are being altruistic usually benefit from doing so. After all, humans are social beings and survived due to that very fact.
For example, it is generally against our nature to kill other people, and we can see that values like that have underpinned many changes in different societies.

## Privacy

The boundaries of privacy are very subjective. Subjective in the sense that you have different pain thresholds, how much is available online, how much you care about protecting your own data and how necessary it is to protect your own data.
I think privacy is the ability to control your data. And controlling your data means controlling your life. 
Nowadays, our everyday lives are built on the data that is available about us.

Insurance fees use information Google, Facebook, cars and more are collecting about you. What you post online might influence which job you get, or you might get persecuted by your government. 

Even those who have the possibility of a democratic state might need to keep that in mind. What is legal today might not be legal tomorrow. Regimes come and go, but your data persists throughout. 
Someone like Trump says he would only be a "dictator for the first day". But who tells you that you won't live in an authoritarian society in the future?

Privacy enables personal freedoms. It is an essential right and without it, our personal freedoms, free discussion, and democracy are in danger.

--> [Post on Privacy](/blog/my-privacy-journey)

## Human Rights

Finally, human rights should be the foundation for the actions of society and states. It is essential to have civil courage. We need to protect children, provide education, prevent the death penalty, and preserve as well as enforce the rights of the LGBTQIA+ community. 
But this also includes taking action against climate change and democracy-hostile ideologies, such as fascism.

I will address some, if not all, of these points in future blog posts. Do you have a different opinion on any of these points? Or some additional ideas? Please let me know.
