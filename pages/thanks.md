---
layout: default
title: Acknowledgments
keywords: thanks, acknowledgments, credits, acknowledgment page,
description: Thanks to all the amazing people who helped me make this site possible.
permalink: /thanks.html
csp: default-src 'self';
---

A heartfelt thank you to everyone who has contributed to the development and growth of this site. Your support and resources have been invaluable!
Please check them out <3!!

## Website

- **Host**: [Neocities](https://neocities.org) - A fantastic platform that hosts this site
- **Discovery**: Found Neocities thanks to [Thore](https://thoree.neocities.org) - Speciall thanks for the constant aid!
- **Website Layout Inspiration**: [sadgrl.online](https://sadgrl.online) - also hase a button creator
- **Static Site Generator**: [Jekyll](https://jekyllrb.com) - Powers the blog with its static site capabilities
- **My button**: I created my button using [Pixilart](https://www.pixilart.com/) and using [sadgrl.online's](https://sadgrl.online) and [Hekate's](https://hekate.neocities.org/) button creator
- **Domain**: I registered my domain through hosting.de, which is a company based in Germany. The domain is new but they seem pretty competent so I am optimistic that I can recommend them
- **Neko**: Neko following cursor by WebNeko: <https://webneko.net/> 😻
- **New Guestbook**: By CactusChat <https://cactus.chat/>, powered by the [Matrix Protocol](https://matrix.org/)
- **Old Guestbook**: Thanks to Ayano who provided the awesome guestbook template <https://virtualobserver.moe/>!
- **Version Control**: Organized the programming using Git and [GitLab](https://gitlab.com/).
- **Information Banner**: [Dom's Design](https://codepen.io/dcode-software/pen/yLaJKYq) on CodePen.
- **Old Blog Design**: Old Blog design by [SANJIB KUMAR DEY](https://codepen.io/sanjib104/pen/JjWrbYq).

### Learning Resources

- **HTML + CSS Tutorials (English)**:
  - [sadgrl.online](https://sadgrl.online) - For beginner-friendly guides
  - [Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web/) - Comprehensive web documentation
  - [W3Schools](https://www.w3schools.com/) - Helpful for quick references
- **HTML + CSS Tutorials (German)**:
  - [selfhtml](https://wiki.selfhtml.org) - Detailed tutorials and guides
  - [Morpheus Tutorials - YouTube link](https://www.youtube.com/@TheMorpheusTutorials) - tutorials to everything reagirding CS

## Images

- **Header Pixel Screen**: [Freepik](https://www.freepik.com/free-vector/vhs-effect-background_37443190.htm)
- **Entrance Screen TV-Glitch**: [TV-Glitch on Tenor](https://tenor.com/de/view/tv-glitch-no-signal-gif-16431868)
- **Static TV Glitch**: [Image by rawpixel.com](https://www.freepik.com/free-photo/glitch-effect-black-background_15559424.htm) on Freepik
- **SVG Images of Icons**: Bootstrap
- **Zebra Guardian**: <https://i.ibb.co/pQ5VvhG/zebra.jpg>
-  **Astolfo Image**: For the self-insert webring by [Nyanachi](https://emoji.gg/user/313058220824461335) on <https://emoji.gg/emoji/5812_astolfo>
- **Self-Insert Webring BG**: Background for the self-insert webring source: <https://koinuko.pink/shipping/webring/bg.png>
- **GitLab Repo Blinkie**: template by [amy](https://graphics-cafe.tumblr.com/) on [blinkies.cafe](https://blinkies.cafe) - img in question [here](/assets/images/blinkies/gitlab-repo.gif)
- **About me Buttons**: from <https://www1.flightrising.com/forums/cc/3078623>

- **City Background**: [inbevel13 on Freepik](https://www.freepik.com/free-vector/night-city-background-with-reflection_848286.htm)
<!-- - **Old Background for Page**: [rawpixel.com on Freepik](https://www.freepik.com/free-photo/glitch-effect-black-background_15559424.htm) -->
- **Halloween Themed Background**: [Bing Image Creator](https://www.bing.com/images/create)

### GIFs

- **C3PO-This is madness gif**: [Star Wars GIF on Tenor](https://tenor.com/view/madness-c3po-c3p0-star-wars-sw-gif-5027419)
- **Padme - Bad Path for 404 Page gif**: [Padme GIF on Tenor](https://tenor.com/view/star-wars-padme-natalie-portman-path-i-cant-follow-gif-10215592)
- **Star Background for short story page**: by [goblin-heart](https://goblin-heart.net/sadgrl/webmastery/downloads/tiledbgs)
- **Cat under Star**: <https://i.gifer.com/7MoV.gif>


### Buttons and Blinkies

- All (neocitie) site-buttons designs are by their respective owner - definitly check them out
- Blinkies are by [Blinkies Café](https://blinkies.cafe)
- All non neocities-site-buttons are from different sites of the neocities community and me

[See all buttons and blinkies](/misc/the_stuff.html)

## Content

- **Translations**: All translations are done by myself, with assistance from [DeepL](https://www.deepl.com/) and sometimes different AI models provided via [DuckDuckGo AI Chat](https://duckduckgo.com/aichat). Each translation is reviewed by me for accuracy. If you find any inaccuracies, please [let me know](/about#socials).

Your feedback and suggestions are always welcome as they help in improving the site. Thank you again to everyone who has contributed in any way. E.g. whos stuff I used 😅, you're all awesome!!

I only have rights for **my** content, not on the content I didn't make. These are for example text excerpts or most images. I try to be as transparent about the origin as possible.

Example for my content would be all blog posts. Those are written by me and free for anyone to use.
I will improve the transperency through other means soon.
