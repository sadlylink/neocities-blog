[![CC0](https://img.shields.io/badge/content-CC0-blue.svg)](LICENSE_CONTENT.txt)
[![Unlicense](https://img.shields.io/badge/code-Unlicense-blue.svg)](LICENSE.txt)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-1.4-4baaaa.svg)](CODE_OF_CONDUCT.md)
![Tested on Firefox](https://img.shields.io/badge/tested%20on-Firefox-orange.svg)

# Hello There 👋

Welcome to the source code repository of my personal blog <https://sadly.link>. This repository enables me to work on my blog from various systems and possibly share and collaborate with others.
The main repo for this project is on [GitLab](https://gitlab.com/sadlylink/neocities-blog), but it is also mirrored on [Codeberg](https://codeberg.org/sadlyLink/neocities-blog). 
On Codeberg some more Commits are marked as verified. This is because I accidentally deleted an older SSH-Key on GitLab - just in case anyone wonders.

## Project Overview

This blog is built using the static site generator [Jekyll](https://jekyllrb.com) and is hosted on [Neocities](https://neocities.org). The foundational layout was adapted from [sadgrl.online](https://sadgrl.online).

(Note for me: Palette for the button: https://coolors.co/palette/eeff01-f20bf1-bb3ef3-fdc012-50a1f9 → use more on site)

### Roadmap 

Upcoming are new themes for this site's pages:
- base theme used on:
    - /home 
    - /blog
    - all posts
- standalone *thanks* theme
- ~~/about~~
- ~~/misc/lists~~

**and** adding shrines for stuff I love

### Structure

This section provides an overview of the main directories and files in the project:

- `/_drafts`: Contains draft blog posts that are not yet published (not published here yet)
- `/_posts`: Stores all published blog posts, categorized into folders like `blog`, `cpp`, etc.
- `/_sass`: Includes Sass (SCSS) files for styling, divided into components, layouts, utilities, etc.
- `/_layouts`: Contains the HTML templates for different types of pages on the blog
- `/_includes`: Houses reusable HTML components like headers, footers, and navigation bars
- `/assets`: This directory has all static assets like CSS, JS, and images. Subdirectories are organized by type (e.g., `css`, `js`, `images`)
- `/pages`: includes structure for most sites, meaning the enter page, home, 404, misc pages, about pages, blog files, etc.
- `ring.txt` and `project_structure.txt`: Special files used for external validation services. E.g. ring.txt is for a webring.
- `Gemfile` and `Gemfile.lock`: Used for specifying and locking gem dependencies for Ruby
- `CODE_OF_CONDUCT.md`, `LICENSE.txt`, `LICENSE_CONTENT.txt`: Contains the code of conduct, software license, and content license information

Each directory is structured to keep the project organized and maintainable, ensuring efficiency in development and content management.

### Priorities

Managing a personal project solo can be challenging. Here are my key goals:

1. **Correctness**: Ensuring the site functions properly
2. **Privacy**: Prioritizing privacy-respecting alternatives over services like Google and no unnecessary JS code
3. **Accessibility**: I aim to make this site more accessible for screen readers and those without a mouse, though compromises have been made for mobile phone functionality. I try to follow [Mozilla's guide for accessibility](https://developer.mozilla.org/en-US/docs/Learn/Accessibility)
4. **Efficiency**: Maintaining a manageable structure when adding new folders and files
5. *W3C Compliance*: I try to follow the W3C standards as far as is reasonable and possible - [online check](https://validator.w3.org/nu/)
6. *Performance*: though important, it is not my main concern. If any of the above conflicts with this goal, it's likely I will do it anyway. The main reason I care is for more sustainable web design

## How to Run

Install the needed gems from the `Gemfile`
```bash
bundle install
```

Updated all gems with:
```bash
bundle update
```

For a live preview run:
```bash
bundle exec jekyll s
```
You can find the live preview at `localhost:4000`

To build the site run:
```bash
bundle exec jekyll b
```
This ensures that the sitemap and feed use `https://sadly.link` (or whatever you added to the `_config.yml` file) instead of `localhost:4000` and the `./_site` folder is ready for deployment.

To test the HTML code of the site, use the `test.sh` script. This script executes `bundle exec jekyll` build, followed by `htmlproofer ./_site`:
```bash
bash test.sh
```

## How to Contribute

Thank you for considering contributing to sadly.link ❤️

> Every contribution needs to follow the [Code of Conduct](CODE_OF_CONDUCT.md)
> By contributing to this site, you agree that your contributions to the code will be in the public domain. For more information look up the [license section](Readme.md#license)

While this is a personal project, suggestions and ideas for improvements are always welcome. If you have something to contribute:

- **Report Issues**: If you have suggestions or encounter any bugs, feel free to open an issue in the repository.
- **Propose Changes**: To propose changes, please fork the repository, make your modifications, and submit a merge request.
- **Discuss Major Changes**: For substantial changes or enhancements, I recommend opening an issue first to discuss your ideas before proceeding with a merge request.

All contributions, regardless of size, will be acknowledged in the project's documentation and on the acknowledgment page of the site. If you prefer a different form of acknowledgment or wish to remain anonymous, please let me know.

## Contact

If you want to get in touch with me, please write me an email at [info@sadly.link](mailto:info@sadly.link). You can encrypt the email using my [PGP-Key](https://mail-api.proton.me/pks/lookup?op=get&search=info@sadly.link). You can find more information at <https://sadly.link/about/>

## License

### Content License

Unless otherwise noted, the creative content of this project, including blog posts, short stories, and other original creative works, is licensed under the [Creative Commons CC0 1.0 Universal (CC0 1.0) Public Domain Dedication.](LICENSE_CONTENT.txt) <br>
This means you can use, modify, and distribute this content freely, without any need for attribution. However, **attribution is appreciated.**
[License site](https://creativecommons.org/publicdomain/zero/1.0/)

### Source Code License

The underlying source code used to format and display the content, including HTML, CSS, JavaScript, etc., is in the public domain. Please find the ["Unlicense" here.](UNLICENSE.txt) <br>
This (un)license allows for the reuse, modification, distribution, etc. of the code.
<https://unlicense.org/>

### Third-Party Content

Please note that the above licenses apply only to the content and code created and owned by me. They do not extend to any third-party works (such as images, excerpts from published works, etc.) featured on this website. 
These works are subject to their own copyright and licensing terms as specified by their respective owners or licensors. Users must adhere to these terms for any use of third-party content.

