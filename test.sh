#!/bin/bash

# Error handeling
set -e

echo "Building Jekyll site..."
bundle exec jekyll build

echo "Running HTMLProofer..."
htmlproofer ./_site

