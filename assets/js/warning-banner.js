document.addEventListener('DOMContentLoaded', () => {
    const closeBannerButton = document.getElementById('closeBannerButton');
    const spoilerBanner = document.getElementById('spoilerBanner');

    if (closeBannerButton && spoilerBanner) {
        closeBannerButton.addEventListener('click', () => {
            spoilerBanner.style.display = 'none';
        });

        closeBannerButton.addEventListener('keydown', (event) => {
            if (event.key === 'Enter') {
                spoilerBanner.style.display = 'none';
            }
        });
    } else {
        console.error('Required elements not found');
    }
});