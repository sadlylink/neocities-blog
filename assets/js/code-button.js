document.addEventListener("DOMContentLoaded", function() {
    const copyButtons = document.querySelectorAll(".copy-btn");

    copyButtons.forEach(button => {
        button.addEventListener("click", function() {
            const targetId = this.getAttribute("data-target");
            const codeElement = document.getElementById(targetId);

            if (codeElement) {
                const code = codeElement.innerHTML; // Use innerHTML to include HTML tags
                navigator.clipboard.writeText(code).then(() => {
                    alert("Code copied to clipboard!");
                }).catch(err => {
                    console.error("Failed to copy code: ", err);
                });
            } else {
                console.error("Code element not found");
            }
        });
    });
});