// by me: enter stuff and easter eggs

// Function to handle the "Enter" key press
function handleEnterKey(event) {
    if (event.key !== "Enter") return;

    processCommand();
}

// Function to handle the "Enter" button click
function handleEnterButtonClick() {
    processCommand();
}

// Function to process the command
function processCommand() {
    const commandInput = document.getElementById('command-line');
    if (!commandInput) {
        console.error('Command input element not found');
        return;
    }

    const command = commandInput.value.trim().toLowerCase();
    const outputDiv = document.getElementById('output');
    if (!outputDiv) {
        console.error('Output div element not found');
        return;
    }

    const outputText = document.createElement('p');
    const commandMap = getCommandMap();

    if (command in commandMap) {
        executeCommand(commandMap[command]);
    } else {
        outputText.textContent = `[user@website] ~$ ${sanitizeInput(command)}`;
        outputDiv.appendChild(outputText);
    }
    commandInput.value = ''; // Clear input after processing command
}

// Function to get the command map
function getCommandMap() {
    return {
        '': '/home/',
        'blog': '/blog/',
        'home': '/home/',
        'start': '/home/',
        'about': '/about/',
        'misc': '/misc/',
        'short story': '/misc/The_Dream_You_Give',
        'positions': '/about/positions.html',
        'guestbook': '/guestbook',
        'lists': '/misc/lists',
        'privacy': '/privacy-policy',
        'help': 'help',
        '-h': 'help',
        '--help': 'help',
        '42': 'https://yewtu.be/watch?v=HacqfsV7ug0',
        '73': 'https://yewtu.be/watch?v=aboZctrHfK8',
        'link start': 'https://yewtu.be/shorts/16J6Wnvsy3k',
        'mellon': 'alert',
        'alerta alerta': 'antifascista',
        'matrix': 'https://www.yewtu.be/watch?v=O5b0ZxUWNf0',
        'hello there': 'https://www.yewtu.be/watch?v=rEq1Z0bjdwc&pp=ygULaGVsbG8gdGhlcmU%3D'
    };
}

// Function to execute a command
function executeCommand(command) {
    const outputDiv = document.getElementById('output');
    if (!outputDiv) {
        console.error('Output div element not found');
        return;
    }

    const outputText = document.createElement('p');

    if (command === 'alert') {
        alert('You Shall Not Pass');
    } else if (command === 'help') { 
        outputText.innerHTML = `[user@website] ~$ help <br> try entering a page on this site or take a look into the source code`;
        outputDiv.appendChild(outputText);
    } else if (command === 'antifascista') {
        alert('Antifascista!');
    } else {
        window.location.href = command;
    }
}

// Function to sanitize user input
function sanitizeInput(input) {
    const div = document.createElement('div');
    div.textContent = input;
    return div.innerHTML;
}

// Event listener for "Enter" key press
document.addEventListener("keydown", handleEnterKey);

document.getElementsByClassName('js-button')[0].style.display = 'inline-block';

document.getElementsByClassName('js-button')[0].addEventListener('click', handleEnterButtonClick);

// ++++++++ END: enter stuff ++++++++

// ++++++++ BEGIN: last updated and hit count provided and inspired by https://weirdscifi.ratiosemper.com/ ++++++++

// Function to fetch site data
async function fetchSiteData() {
    try {
        const response = await fetch("https://weirdscifi.ratiosemper.com/neocities.php?sitename=code-cafe", {
            credentials: 'omit'
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const siteData = await response.json();
        updateSiteInfo(siteData);
    } catch (error) {
        console.error('Error fetching site data:', error);
    }
}

// Function to update site information
function updateSiteInfo(siteData) {
    const numStr = formatNumberWithCommas(siteData.info.views);
    const dateObj = new Date(siteData.info.last_updated);

    updateLastUpdateElement(dateObj);
    updateHitCountElement(numStr);
}

// Function to format number with commas
function formatNumberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// Function to update the last update element
function updateLastUpdateElement(dateObj) {
    const lastUpdateElement = document.getElementById("lastupdate");
    if (lastUpdateElement) {
        const year = dateObj.getFullYear();
        const month = String(dateObj.getMonth() + 1).padStart(2, '0');
        const day = String(dateObj.getDate()).padStart(2, '0');
        lastUpdateElement.textContent = `${year}-${month}-${day}`;
    } else {
        console.error('Last update element not found');
    }
}

// Function to update the hit count element
function updateHitCountElement(numStr) {
    const hitCountElement = document.getElementById("hitcount");
    if (hitCountElement) {
        hitCountElement.textContent = numStr;
    } else {
        console.error('Hit count element not found');
    }
}

// Fetch site data on load
fetchSiteData();

// ++++++++ END: last updated and hit count ++++++++
