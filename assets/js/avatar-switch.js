// Function to switch avatar
function switchAvatar(currentId, nextId) {
    const currentAvatar = document.getElementById(currentId);
    const nextAvatar = document.getElementById(nextId);

    if (currentAvatar && nextAvatar) {
        currentAvatar.style.display = "none"; // Hide the current avatar
        nextAvatar.style.display = "block"; // Show the next avatar
        nextAvatar.focus(); // Focus on the next avatar
    } else {
        console.error('Avatar elements not found');
    }
}

// Function to add event listeners
function addAvatarEventListeners(avatarId, nextAvatarId) {
    const avatar = document.getElementById(avatarId);
    if (avatar) {
        avatar.addEventListener("click", () => switchAvatar(avatarId, nextAvatarId));
        avatar.addEventListener("keydown", (event) => {
            if (event.key === "Enter") {
                switchAvatar(avatarId, nextAvatarId);
            }
        });
    } else {
        console.error(`Avatar with id ${avatarId} not found`);
    }
}

// Add event listeners for each avatar
const avatarPairs = [
    ["avatar1", "avatar2"],
    ["avatar2", "avatar3"],
    ["avatar3", "avatar4"],
    ["avatar4", "avatar5"],
    ["avatar5", "avatar1"]
];

avatarPairs.forEach(([currentId, nextId]) => addAvatarEventListeners(currentId, nextId));