document.addEventListener("DOMContentLoaded", async function() {
    try {
        const response = await fetch('/assets/quotes.json');
        const data = await response.json();
        const randomQuote = data[Math.floor(Math.random() * data.length)];
        const quoteElement = document.getElementById('gr_quote_body');

        if (quoteElement) {
            quoteElement.innerHTML = `"<span title="${randomQuote.book}">${randomQuote.quote}</span>"&mdash; <a href="https://en.wikipedia.org/w/index.php?search=${randomQuote.author}" title="Quote by" target="_blank">${randomQuote.author}</a>`;
        } else {
            console.error('Quote element not found');
        }
    } catch (error) {
        console.error('Error fetching quotes:', error);
    }
});