 // ++++++++ Collapsible ++++++++
document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll(".collapsible").forEach(coll => {
        coll.addEventListener("click", () => {
            coll.classList.toggle("active");
            const content = coll.nextElementSibling;
            content.style.display = content.style.display === "block" ? "none" : "block";
        });
    });
});