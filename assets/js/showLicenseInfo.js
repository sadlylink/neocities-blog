function showLicenseInfo(element) {
    var info = element.querySelector('.license-info');
    var isVisible = info.style.display === 'block';
    info.style.display = isVisible ? 'none' : 'block';
}

// document.addEventListener("DOMContentLoaded", function() {
//     var elements = document.querySelectorAll('.game-item');
//     elements.forEach(function(element) {
//         element.addEventListener('click', function() {
//             showLicenseInfo(element);
//         });
//     });
// });