// +++++++++ Code inspired by Status Cafe: https://status.cafe/current-status.js +++++++++

// Constants for element IDs
const STATUS_CAFE_ID = 'statuscafe';
const USERNAME_ID = 'statuscafe-username';
const CONTENT_ID = 'statuscafe-content';
const PARENT_ELEMENT_ID = 'statuscafe-container';

// Create a div container for the status cafe
const statusCafeContainer = document.createElement('div');
statusCafeContainer.id = STATUS_CAFE_ID;

const usernameDiv = document.createElement('div');
usernameDiv.id = USERNAME_ID;

const contentDiv = document.createElement('div');
contentDiv.id = CONTENT_ID;

statusCafeContainer.appendChild(usernameDiv);
statusCafeContainer.appendChild(contentDiv);

// Append the status cafe container to a specific parent element
const parentElement = document.getElementById(PARENT_ELEMENT_ID);
if (parentElement) {
  parentElement.appendChild(statusCafeContainer);
} else {
  console.error('Parent element for status cafe not found.');
}

// Function to fetch the status from the status cafe API
const fetchStatus = async () => {
  try {
    const response = await fetch("https://status.cafe/users/code-cafe/status.json", {
      credentials: 'omit' // Do not send cookies
    });
    
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }

    const data = await response.json();
    const { author, face, timeAgo, content } = data;

    const usernameElement = document.getElementById(USERNAME_ID);
    const contentElement = document.getElementById(CONTENT_ID);

    if (!content || content.length === 0) {
      contentElement.textContent = "No status yet.";
      return;
    }

    usernameElement.innerHTML = `
      <a href="https://status.cafe/users/code-cafe" target="_blank">${author}</a> 
      ${face} 
      ${timeAgo}
    `;
    contentElement.textContent = content;
  } catch (error) {
    console.error('Error fetching status:', error);
    document.getElementById(CONTENT_ID).textContent = "Failed to load status.";
  }
};

// Call the fetch function
fetchStatus();

