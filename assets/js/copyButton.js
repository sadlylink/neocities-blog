function copyFunc() {
    try {
        // Select the text from the element with the class 'copyButton'
        const copyText = document.querySelector('.copyButton');
        copyText.select();
        copyText.setSelectionRange(0, 99999); // Select the entire range of text

        // Write the selected text to the clipboard
        navigator.clipboard.writeText(copyText.value).then(() => {
            // Change the tooltip text to "Thank you!" after the text has been copied
            const tooltip = document.getElementById("myTooltip");
            tooltip.textContent = "Thank you!";
        }).catch(err => {
            console.error('Failed to copy text: ', err);
        });
    } catch (err) {
        console.error('Error selecting text: ', err);
    }
}

// This function is used to reset the tooltip text to "Copy to clipboard" when the mouse is no longer hovering over the button
function outFunc() {
    const tooltip = document.getElementById("myTooltip");
    tooltip.textContent = "Copy to clipboard";
}