// Pause and play the conveyor belt on hover
document.addEventListener('DOMContentLoaded', () => {
    // Get all the ul elements
    const ulElements = document.querySelectorAll('.box ul');
    ulElements.forEach(ul => {
        ul.addEventListener('mouseover', () => {
            // Pause the animation on mouseover
            ul.style.animationPlayState = 'paused';
        });
        ul.addEventListener('mouseout', () => {
            // Play the animation on mouseout
            ul.style.animationPlayState = 'running';
        });
    });
});