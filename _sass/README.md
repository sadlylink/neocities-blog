# SCSS Structure

This project uses SCSS for styling. The SCSS files are organized into several directories, each with a specific role.

## Directory Structure

- `main.scss`: The main SCSS file that imports (all) other SCSS files - some are specific for 404, about misc etc.
- `_variables.scss`: Contains SCSS variables, such as colors, fonts, etc.
- `_components.scss`: Contains styles for smaller components used throughout the site like `.box`, `.conveyor`, `.tooltip` and `.note`

- `base/`: Contains global styles, such as resets, typography, etc.
    - `_global.scss`: Global styles and resets
    - `_typography.scss`: Typography rules and font declarations

- `layout/`: Contains styles for major site sections (header, footer) and for the site layout.
    - `_dropdown.scss`: Styles for dropdown menus.
    - `_footer.scss`: Styles for the footer.
    - `_header.scss`: `#header` for the image in the header
    - `_layout.scss`: General layout styles.
    - `_navbar.scss`: `#navbar` for menu navigation and highlighting. !Imporve accessebility!
 - `pages/`: Contains page-specific styles.
    - `_404.scss`: This file contains styles for the 404 page. It includes classes like `.search-404` for the search box on the 404 page, `.search-input` for the input field, and `.search-button` for the search button.
    - `_about.scss`: This file contains styles for the About page. It includes classes like `.about-site` for the main about section, `.avatar` for the avatar style and `.aLinkToThePast` for the social links. It also styles elements like `li` for list items and `a` for links.
    - `_blog.scss`: This file contains styles for the Blog page. `.post-list` for the list of posts, and `.card` for the blog post cards. It also styles elements like `img` for images and `a` for links. It also includes classes like `.post-metadata` for the dates at the top of a posts itself
    - `_home.scss`: This file contains styles for the Home page. It includes classes like `.split-view-container` for the main container, `.left-panel` and `.right-panel` for the two panels of the split view, and `.results` with `.zebra` for the hekate web guardian section. It also styles elements like `ul`
    - `_lists.scss` For specific style of the lists in the list page: grid-games, game-item, gr_grid_book_container

- `utils/`: Contains utility classes and helper functions.
    - `_utilities.scss`: Utility classes

- `widgets/`: Contains styles for specific UI components, or "widgets"
    - `_banner.scss`: Styles for banner components
    - `chat-box.scss`: Styles for the chat box by Cbox on /guestbook
    - `_guest-book.scss`: Styles for the guest book component
    - `_piclog.scss`: Styles for the piclog component
    - `_status-cafe.scss`: Styles for the status cafe component
    - `_tldr.scss`: Styles for "too long, didn't read" sections in blog posts
    - `_todo-box.scss`: Styles for the to-do box on the homepage
    - `_update-box.scss`: Styles for the update box component
