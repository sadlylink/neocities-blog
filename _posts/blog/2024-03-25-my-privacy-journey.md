---
layout: post
title: Starting My Privacy Journey
date: 2024-03-23
tags: learning personal
categories: blog
keywords: privacy, learning, journey, privacy journes, imporve privacy, security, human rights, facbook, microsoft, google, apple, proton, personal change, future, data, improving privacy, digital security measures
description: Privacy has become a necessity for controlling your life. I wanted to share how I started to improve my privacy and which resources helped me
excerpt_separator: <!--more-->
image: /assets/images/blog/Surveillance_quevaal.jpg
alt: Surveillance Cameras
credit: CC BY-SA 3.0 DEED <a href="https://commons.wikimedia.org/wiki/File:Surveillance_quevaal.jpg" target="_blank">on wikimedia</a> by <a href="https://commons.wikimedia.org/wiki/User:Quevaal" target="_blank">Quevall</a>
last-edited: 2024-06-16
language: en
permalink: /blog/my-privacy-journey.html
comments: false
---

In today's world, maintaining privacy and security is not just a choice; it's a necessity. <a data-proofer-ignore></a>But why care about privacy if you have nothing to hide?
<!--more-->

It's simple: control and choice. While I agree that "having nothing to hide" is a common sentiment, for me, it's about having the autonomy to decide what stays private and what doesn't. 
Plus, I'm not keen on the idea of being a constant target for monetization and tracking.

But there's more to it than just controlling personal data. Consider how entities like Google, Facebook, and even vehicles collect information that can influence everything from insurance fees to job prospects. What we share online could either open doors or lead to unwarranted scrutiny, sometimes even from government agencies.

And this journey isn't just about safeguarding my data. It's also about protecting those around me. For instance, using apps like WhatsApp means inadvertently exposing my contacts to tracking as well.

And while companies like Apple tout privacy, they are far from being as private as they promote themselfes to be (as highlighted in this Proton article <a href="https://proton.me/blog/iphone-privacy" target="_blank">here</a>).

Also what is legal and what not may change with chaning political systems. But the data that is collected on you stays.
Given that someone like Donald Trump announced he would be a <a href="https://www.theguardian.com/us-news/2023/dec/06/donald-trump-sean-hannity-dictator-day-one-response-iowa-town-hall" target="_blank">dictator for “one day”</a> the comfort of democracy might be short.

So getting started with privacy is an important step to regaining more sovereignty over your life.

## My Starting Point

I first tweaked some settings in the Apps I already use. Especially some privacy-invasive apps like Instagram, Snapchat, WhatsApp and Google needed to change.
My starting point were some resources by <a href="https://tacticaltech.org/projects/" target="_blank">Tactical Tech</a> - their resources are a treasure trove of information, like the project: <a href="https://datadetoxkit.org/" target="_blank">Data Detox Kit</a>

Also instrumental has been the <a href="https://ssd.eff.org/" target="_blank">Surveillance Self-Deffence (SSD)</a> by the <a href="https://eff.org/" target="_blank">EFF</a> (a privacy non-profit), which laid a solid foundation for my privacy education.

## Developing My Threat Model

To progress further, I needed to define my threats and assess my vulnerabilities. The SSD's framework was immensely helpful. I answered the following questions:

1. **Identify Assets**: What am I protecting?
2. **Identify Threats**: Who am I protecting it from? (<a href="https://www.privacyguides.org/en/basics/common-threats/" target="_blank">list of common threats</a>)
3. **Assess Vulnerabilities**: What are my weaknesses?
4. **Assess Risk**: How likely is it that I need to protect it?
5. **Consequences of Failure**: What are the consequences if I fail to protect it?
6. **How far am I willing to go?** How much time am I willing to invest?
7. **Mitigate Risks**: What to do to improve?

What mattered for me was enhancing my privacy without compromising on security. Transitioning from MacOS to more privacy respecting operating systems like Linux had its privacy perks, but I needed to balance it with robust security measures.

## Streamlining Communication and Apps

<figure>
<img src="/assets/images/blog/sw200000units.jpg" alt="Star Wars Meme: 200,000 units are ready with a million more well on the way" id="sw-meme-img">
</figure>
<figcaption>
<small>Image from <a href="https://knowyourmeme.com/photos/1725054-prequel-memes" target="_blank">knowyourmeme by McWooky</a></small>
</figcaption>

My use of iCloud, necessitated by family commitments, is a privacy challenge. Proton has emerged as a viable alternative for me, offering a comprehensive suite of encrypted services. I try to switch to it as my daily mail driver and I deleted my iCloud calendar and use Protons, which works just fine.


Realizing I had nearly a hundred apps on my phone was a wake-up call.

Another step was choosing privacy preserving alternatives for necessary apps. A very helpful tool for this is <a href="https://alternativeto.net/" target="_blank">AlternativeTo.net</a>.


I now got this down to around 50 apps, which is still probably more than I would need.

Besides that, I still rely on services like WhatsApp, Instagram, Discord and Snapchat for social connections. Yet I try to tighten their privacy settings. And I don't use Instagram as a normal App, but as a Web App, that I saved to my homescreen.

An easy win was hardening Firefox with <a href="https://github.com/yokoffing/Betterfox" target="_blank">Betterfox</a>. It's a simple process - just a file to copy and paste. And alternatives like Brave support good privacy out of the box (though I am not such a big fan of Brave Browser).

## Rethinking My Linux Choice

While I use MacOS as my daily driver, I also love Linux and it's open source and community-driven ethos.

Arch Linux encorperates everything I like about Linux. You are in control over everything you install, it is community driven and super costimizable. It is definitly one of the best Linux distros out there. I used Arch for some time now, and though I still love the distro its demanding maintenance, didn't align with my need for a 'just works' system.

Fedora became my new choice, thanks to its user-friendly setup and pretty guide security out of the box.
I'll probably use Arch Linux again sometime but for now I need more time for other things than my operating system.

## Looking Ahead

There's always room for improvement. My next phone might feature a more private OS. 
A big privacy concern for me ofcourse is this personal blog. I try to be carefull what I share here as well as trying to provide links to privacy-respecting sites like <a href="https://invidious.io" target="_blank">Invidious</a> instead of YouTube. 
I also added PGP encryption to my email using ProtonMail so you can write me encrpyted (see [about](/about/))

While moving away from iCloud isn't feasible right now, minimizing its usage is a step in the right direction. The most critical part of this journey has been educating myself and staying informed.
Especially the resources below helped me achieve that, so check them out!

<!-- Edit: I switched to GrapheneOS, read some information about this in [my guide](/guides/graphene-os) -->

<div class="tldr"><strong>Resources</strong>
<ul>
<li><a href="https://tacticaltech.org/projects/" target="_blank">Tactical Tech</a>: For comprehensive privacy projects</li>
<li><a href="https://yewtu.be/channel/UCSuHzQ3GrHSzoBbwrIq3LLA" target="_blank">NBTV</a>: Covering privacy and security topics</li>
<li><a href="https://www.eff.org/" target="_blank">EFF</a>: An non-profit privacy organization</li>
<li><a href="https://www.privacyguides.org/en/" target="_blank">Privacy Guides</a>: For resources and recommendations</li>
<li><a href="https://alternativeto.net/" target="_blank">Alternative To</a>: Find alternatives for almost all apps</li>
<li><a href="https://foundation.mozilla.org/en/privacynotincluded/">*privacy not included</a> For product reviews on privacy and security</li>
<li><a href="https://justdeleteme.xyz/">Just Delete me</a> For deleting unwanted accounts</li>
<li>Book: <b>Privacy is Power</b> by <i>Carissa Véliz</i></li>
</ul>
</div>
