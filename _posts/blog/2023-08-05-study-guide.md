---
layout: post
title: Unraveling the Science of Efficient Learning
date: 2023-08-05
tags: learning science
categories: blog
keywords: learn, science, procrastination, school, university, college, make it stick, guide
description: Let's face it - Sometimes we have to learn something for school, university, work, etc. and it is just overwhelming. But what does really help?
excerpt_separator: <!--more-->
image: /assets/images/blog/question-mark-Sobsz.svg.png
alt: First page of study guide
credit: CC0 by <a href="https://commons.wikimedia.org/wiki/File:Sobsz_-_gender_question_mark.svg" target="_blank">Sobsz</a> on wikimedia
last-edited: 2024-08-19
language: en
permalink: /blog/study-guide.html
comments: false
---

## Study Guide

Let's face it. Sometimes we have to learn something for school, university, work, etc. and it is just overwhelming.<a data-proofer-ignore></a>
Personally, I have often struggled with procrastination and wanted to be more “efficient”. But most productivity tricks
seemed to be just bullshit.
<!--more-->

Because of that, a friend and I created a study guide. It is based on actual scientific results.
If you want to dig deeper into the matter I highly recommend getting the book [»Make it Stick!«](https://www.makeitstick.com/).

<a href="/assets/images/blog/learning/study_guide.jpg"><img src="/assets/images/blog/learning/study_guide.jpg" alt="Study Guide 1" class="study-img"/></a>

<a href="/assets/images/blog/learning/studyguide2.jpg"><img src="/assets/images/blog/learning/studyguide2.jpg" alt="Study Guide 2" class="study-img"/></a>
