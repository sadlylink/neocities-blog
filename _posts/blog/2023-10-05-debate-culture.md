---
layout: post
title: Bigotry, Indignation, and Utopia - Boundaries of Debate
date: 2023-10-05
tags: philosophy debate
categories: blog
keywords: debate, discussion, my opinion, philosophy, power, violence, ContraPoints, Coffee, Tea, bigots, J. K. Rowling, Neocities, lgbtqia+, trans right, human rights
description: What is allowed against bigotry and descrimination? How far can or should one go. Grab a cup of coffee or tea and let’s talk about debate culture…
excerpt_separator: <!--more-->
image: /assets/images/blog/debate_speech_bubbles.svg
alt: three speech bubbles
credit: CC BY-SA <a href="https://commons.wikimedia.org/wiki/File:Speech_balloon_3_types.svg">on Wikimedia</a> by EnEdC
last-edited: 2024-07-05
language: en
permalink: /blog/debate-culture.html
listed: https://listed.to/@sadlyLink/52723/bigotry-indignation-and-utopia-boundaries-of-debate
comments: false
---

## Confronting Bigotry

Imagine a world where debates effortlessly pave the way for understanding, where diverse perspectives interweave seamlessly.
<a data-proofer-ignore></a> Yet, in reality, our discourse landscape is a battlefield adorned with complexities.
<!--more-->

I recently came across a cool video by ContraPoints: [«The Witch Trials of J.K. Rowling»](https://yt.artemislena.eu/watch?v=EmT0i0xG6zg).
It's basically a response to a podcast but delves deep into debate culture and the matter of JK Rowling.

One passage stuck with me:
> We have to accept that realistically, persuading all the bigots is just not an option. <br>
Yes, we should convince as many people as possible, but there will always be bigots, and mocking them, shaming them, or boycotting them is, I think, a perfectly valid strategy. […] <br>
I guess, controversial opinion, but bigotry is shameful.
And it should be shamed. <br>
(53:39)

This excerpt from the video refers to Anita Bryant, who vehemently opposed equal rights for gay people. There are many parallels to J.K. Rowling, who does the same with trans rights.
I definitely recommend you check out the video.

But the question arises: is mocking, shaming, or boycotting actually a valid strategy against bigots, who won’t change their minds through debate?

Grab a cup of coffee or tea – as you please – and let’s talk about debate culture…

### The Challenge of Marginalised Voices

When you belong to a marginalised group and experience discrimination, seeking change in society is paramount, and this goes beyond engaging in debates with individuals who are beyond a point of no return for changing their minds.

This means, that not everyone has the privilege to engage in a «calm and factual» discussion on equal footing when their very existing is put under interrogation by someone who won't change their mind through the best argument.

One could argue that shaming or boycotting can sometimes entrench people in their beliefs and create more division.
However, historical examples, such as the struggle for women's suffrage and the fight for LGBTQ+ rights, highlight that marginalised groups have to face prolonged battles for equal rights due to bigotry. Fighting back was and will be a catalyst of change. <br>
Beyond bigotry, **power imbalances** also pose problems. <br>
E.g. the issue of power imbalance extends on a smaller scale to the challenge young people face in gaining more rights and opportunities in politics.

---
<div class="tldr"><strong>TL;DR</strong> Discrimination, bigotry and power imbalances are making a discussion on equal footing challenging or even absurd</div>

### The Power Imbalance

This fundamental aspect of debates: the power imbalance, makes it often difficult to discuss. No one wants a disadvantage in debate (or society).
Or as a friend of mine put it: [«The goal of a debate is to win.»](https://thoree.neocities.org/blog/entry/debateculture)

Those in power typically make decisions that don't negatively affect them. This power dynamic is posing a significant obstacle to achieving solutions for various issues.

The philosopher and political theorist Hannah Arendt wrote about the nature of power and violence in her book ‘On Violence’. <br> She argued that violence and power are diametrical. Power stems from the ability to act collectively, while violence is the use of force to impose one's will. This distinction can lead to situations of «all against one» in power-based systems and «one against all» in totalitarian regimes.

Nonetheless, a democratic state is better than a totalitarian one. The way one can prevent that power from being misused against minorities is by – surprise – fighting against this misuse.

---
<div class="tldr"><strong>TL;DR</strong> Power imbalances hinder debates and decision-making, so they need to be addressed.</div>

### A Utopian Ideal and Indignation

In an ideal world, we could all engage in discussions on an equal footing, echoing Jürgen Habermas’ **«The Unforced Force of the Better Argument»**.
However, our society frequently fails to attain this ideal, where inequality persists. Injustice all too often necessitates indignation.
<p class="note">Stéphane Hessel's call to action, «Indignez-vous!» resonates strongly here. In his essay, Hessel - a Résistance fighter, concentration camp survivor and co-author of the Universal Declaration of Human Rights – urges being indignant and standing up against inequality and injustice. <span class="more-info">«To resist is to create. <br> To create is to resist.»</span></p>
As he notes, it is not as clear today as it was in his days what constitutes an outrage. But this shouldn’t be an excuse for inaction.

He highlights two major issues of the 21st century that need to be addressed:
1. The wealth gap
2. The state of Human rights and our planet

---
<div class="tldr"><strong>TL;DR</strong> Ideal discussions involve equal participation, but the reality falls short. Indignation can be a powerful catalyst for change.</div>

### In a nutshell

<div style="background-color: #13092D; border: 1px solid #ED64F5; color: lightgreen; padding: 5px;">
Debate culture is intricate and choosing which debates to engage in requires thoughtfulness. The crux lies in discussions rooted in the idea of Habermas' "unforced force of the better argument." Meaningful dialogues that seek truth and understanding are paramount. However, discussions solely steeped in bigotry, hatred, or the denial of existence for certain groups deserve indignation as a starting point for change. But this is not to legitimise violence or hatred.
Aiming for the better argument is a noble goal (IMO), but one must confront bigotry, power imbalances, and systemic injustices.
</div>
