---
layout: post
title: Zelda and the Apes
date: 2024-06-11
tags: videogames movies zelda
categories: blog
keywords: movies, zelda, link, the legend of zelda, apes planet of the apes, films, new movie, kingdom of the planet of the apes
description: The new Kingdom of the Planet of the Apes movie is out since a few weeks. My thoughts and what this means for The Legend of Zelda
excerpt_separator: <!--more-->
image: /assets/images/blog/zelda_title.jpg
alt: The Legend of Zelda logo
credit: CC0 | Original <a href="https://commons.wikimedia.org/wiki/File:Zelda_2017.svg" target="_blank">by Nintendo</a>
last-edited: 2024-06-11
language: en
permalink: /blog/zelda-and-apes
comments: false
---

## The new Planet of the Apes movie makes me optimistic about Zelda
I recently watched the new Movie: **Kingdom of the Planet of the Apes!**<a data-proofer-ignore></a> 

It is a movie by director Wes Ball, who will be making the upcoming **The Legend of Zelda movie**, and this movie might be a sneak peek at what he has to offer!
<!--more-->
<br>
It is the fourth movie of the new Planet of the Apes iteration and marks the first in a probably new trilogy.

**Disclaimer**: I am not sure how much was teased in the trailers, so just a genuine spoiler warning if you haven't seen "Kingdom of the Planet of the Apes", but there won't be any heavy spoilers. Still, if you want to watch the movie without any spoilers whatsoever, please read this afterwards.

Also, I haven't seen the first three movies, and from what I can tell this is not needed, but gives more additional context. So keep that in mind.
And if you have missed it, last November a Zelda live-action movie was announced. <br> 

I wrote some of my thoughts, hopes and fears down here when the announcement came out. You can find the [post here](/blog/zelda-movie.html).

The directors of the Zelda movie are Avi Arad and Wes Ball. Arad is responsible for the Sony superhero movies like the Spider-Man movies. And Wes Ball made the "Mace Runner" movies and "Kingdom of the Planet of the Apes"

### The movie

The movie is set "many generations" after the first three movies, roughly about 300 years. 

Still, the first movies are heavily referenced. Around the teachings of the protagonist - *Caesar* - of the previous three movies, some legends and cults have formed.<br>
<p>This gives you, if you have seen the previous movies, plenty of "Oh I know what this means, I have SEEN IT!!!" moments (please don't shout this in the theatre). This also created the problem that we have <i>Caesar</i> from the first 3 movies and now a new <i>Caesar</i>, <i>Proximus Caesar</i>, twists <i>old Caesars</i> teachings for his advantages.<br></p>

#### The Story

Starting with what is not so compelling: the story.

<div class="content-container-posts">
    <div class="text-content-posts">
        <p>
            I mean, the story is <i>okay</i> and today mediocrity is harder to achieve than making a good or a bad movie. The writing was okay and nothing more.
        </p>
        <p>
            Hollywood made a generic Hollywood movie with awesome visuals. Does it function? Yea, somewhat. But don't expect anything super brilliant.
        </p>
        <p>
            The protagonist of this movie is Noah. Noah is a very average protagonist. Somewhat intelligent, and good at the physical stuff, and he gets a tragic motivation to go on an adventure. <br>
            He is accompanied by Mae, an intelligent human, which is rare, or at least that's what we are told in the movie. <br>
            Because after a virus spread, Apes became intelligent and humans, well more stupid. Switching roles of Apes and Humans. Now Humans are hunted by Apes. 
        </p>
        <p>
            It could have been "brilliant", if they had explored more on what the Apes made out of the teachings of <i>Caesar</i> and how changing the motto "Together strong" to "Apes together strong" made the difference between an inspiring figure working with humans and apes to try to do the best to create an authoritarian dictator acting in self-interest.
        </p>
        <p>
            This would be awesome. Add Orwell's "Who controls the past controls the future. And who controls the present controls the past" and this would have been amazing. Maybe play a bit with the hero story more. <br>
        </p>
    </div>
    <div class="image-container-posts">
        <img src="/assets/images/blog/Dan_Harmon_Story_Circle.jpg" alt="The Dan Harmon Cycle - often used in Hero Stories" class="harmon">
        <small>Image from <a href="https://commons.wikimedia.org/wiki/File:Dan_Harmon_Story_Circle.png">wikimedia</a> CC BY-SA 4.0</small>
    </div>
</div>

I didn't expect them to do something as exceptional with the Monomyth (aka the Dan Harmon Cycle) as the Star Wars series "Andor" <b>brilliantly</b> did, making it one of the best series ever. But some more happenstance would've been good for the movie.

The overall plot of the "Planet of the Apes"-movies is common pop culture knowledge and this **shall not** be a review of the movie, more about its implication for the Zelda movie.

**So how does this influence the upcoming Zelda movie?**

For the **"The Legend of Zelda"** movie, I think Nintendo will have a lot to say about where the story is going. <br>

If Nintendo want's to make a live-action adaptation of - let's say "Skyward Sword" - Nintendo will more or less guide how the story is going to unfold. <br>

So I am fairly optimistic that the story will be good. The more important thing is world-building and the visuals of **Hyrule**...

#### The Visuals of a Planet

"Kingdom of the Planet of the Apes" looks awesome. This is James Cameron Avatar-level visual effects.<br>
The apes all look different and are very detailed. Also, props to the performance, because this gave me Goosebumps at some points. <br>
And the setting and look of the world! It. Is. Amazing! <br>
I just wanted to go into that world and wander through every ruin and every field. Ride with my horse through the landscape of this Planet. <br>

This makes it a movie worth to be watched in cinema.<br>

And **this** is what I want from an upcoming Zelda movie. The setting and world-building are what I love about the Zelda games. And seeing the different Apes and the Planet Wes Ball created, I do not doubt that he will take good care in creating awesome Gorons, Zoras, Hylians and so on. <br>

The visuals need to be on point for Zelda, and the more I think about it the more optimistic I am about a live-action adaption because, with this good CGI, it can be amazing.

James Cameron showed that creating Anime Character Protagonists in a movie adaption can function and look good with "Alita: Battle Angel". And man, I love this movie (the mangas were fun too).

So if the acting performance of *Link* and *Zelda* and who might be the bad guy (probably *Ganondorf*, they won't take some no name for the 1. movie) this can look good. Nonetheless, it would be lovely to see a Studio Ghibli-like Anime adaptation of Zelda, maybe in the future...

Back to the Apes and Link's.<br>
This movie already looked a bit like a *Zelda movie*. Of course, we just had apes riding on horses but imagine some other species like Gorons and some Monsters running around, you can see Hyrule. The landscape and the green fields, as well as the ruins from cultures long forgotten all add up to this. <br>

It is impossible to bring the world of **The Legend of Zelda** to live-action without CGI, and Wes Ball is exceptionally good at this.
And I am excited to see what this movie has in store for us.
