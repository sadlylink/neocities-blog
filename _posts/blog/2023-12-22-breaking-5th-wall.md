---
layout: post
title: Breaking the 5th Wall
date: 2023-12-22
tags: writing videogames philosophy
categories: blog
keywords: writing, existencial crisis, ddlc, doki doki literatur club, futurama, toy story, videogames, gaming, 5th wall, 4th wall, absurdism
description: What is the 5th wall, and why is it so intriguing? What fascinates me about the 5th wall, why you should use, and what makes Doki Doki Literature Club so great?
excerpt_separator: <!--more-->
image: /assets/images/misc/lists/Monika.gif
alt: Monika dancing gif by Cjhpaz
credit: CC BY-SA <a href="https://commons.wikimedia.org/wiki/File:ReconcileMonikaIdle.gif" target="_blank">by Cjhpaz</a>
last-edited: 2024-07-05
language: en
permalink: /blog/breaking-the-5th-wall.html
listed: https://listed.to/@sadlyLink/52894/breaking-the-5th-wall
comments: false
---

## Or How to Give Your Audience an Existential Crisis

### Introduction

Recently, I completed Doki Doki Literature Club (DDLC), achieving 100% data collection— a rare feat for me. It is a really great game and one of [my favourites](/misc/lists.html). The only other games I tried to get 100% were a few Zelda games and the Spider-Man series.
<!--more-->

The reason why I love Doki Doki Literature Club so much is because of the idea of the 5th wall. I want to write here what I like about this storytelling device. I heavily recommend the [video by Tale Foundry](https://yewtu.be/watch?v=YrCfwLkspLY) for a more in-depth analysis of this device

### What is the 5th wall?

We're all familiar with the 4th wall – that imaginary barrier between the stage and the audience, in addition to the three real walls that make up the stage.

You can break this wall in various ways, like **“Deadpool”** talking directly to us or Puck in **“A Midsummer Night's Dream”** acknowledging our presence. A bit more subtle are the so-called [alienation effects](https://www.wikiwand.com/en/Distancing_effect) by Bertolt Brecht, aimed to make the audience clear that they are in a theatre.

But there's something even more intriguing: breaking the 5th wall. This happens when what separates the audience from beyond is shattered, creating an immersive and goosebump-inducing experience. If breaking the 4th wall is about recognising the play, then breaking the 5th wall is about transcending the play and the audience altogether.
Leaving us questioned what reality means.

<div class="tldr"><strong>TL;DR</strong> The 5th wall is broken if the imaginative barrier between the audience and what ever lies behind them is shattered to pieces.</div>

---

There are two reasons/aspects why I like breaking the 5th wall in storytelling (if done well):

### 1. Existential Horror

<div class="content-container-posts">
    <div class="text-content-posts">
        <p>Let’s start this with Doki Doki Literature Club. The game itself starts as a dating sim - but quickly turns dark.</p>
        <p>I highly recommend playing the game. It is free, but there is a plus version for some very cool extras <a href="https://ddlc.moe" target="_blank">--> DDLC</a>. If you haven't played it, go now, it's totally worth it!</p>
        <br>
        <p>The characters are aware they're in a game, and so are you. Yet, this knowledge doesn't make the experience less real. The existential horror comes from imagining yourself within the fiction. Each time I play DDLC, the blurring lines between fiction and reality give me chills. </p>
    </div>
    <div class="image-container-posts">
        <img src="/assets/images/misc/lists/Monika.gif" alt="Monika Gif" id="monica-img">
        <small><a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA</a> by <a href="https://commons.wikimedia.org/wiki/User:Cjhpaz" target="_blank">Cjhpaz</a> from <a href="https://commons.wikimedia.org/wiki/File:ReconcileMonikaIdle.gif" target="_blank">wikimedia</a></small>
    </div>
</div>

<p>It's unconventional, immersive, and a perfect example of the 5th wall break.</p>

<div class="tldr"><strong>TL;DR</strong> In exploring existential horror, DDLC masterfully blurs the lines between game and reality and perfectly exemplifying the impact of the 5th wall.</div>

### 2. Free Will and Reality

If you question how real the thing you call reality is, this opens up the question of free will in a new way.

Philosophically, the discussion around if the universe is deterministic or not feels a bit worn out. The idea of living in a simulation and what this means to the worth of living and the “meaning of life” adds a new flavour to this discussion. This used really well in the 10. episode of season 11 from Futurama.

The sci-fi comedy series Futurama recently returned after ten years. In the last episode of the new season, the crew creates a simulation of the world. Then the simulation creates their own simulation, and their simulation also creates a new simulation, and so on. Leaving the crew with the very likely fact that they live in a simulation themselves.

Meanwhile, Bender also gets an existential crisis, because he is a mere simulation of consciousness.
Simulation-Fry then realises that it is not important whether the world is real or not, the emotions are.
Or as Hermes then puts it:

> I feel, therefore I am.

This puts an end to this discussion.

But not just questioning the reality got the discussion around free will and choice, new life (IMO).

The Spider-Verse movies now ask the question if you need to have some events in the life of a Spider-Man to really be him. While the first movie was about “everyone can wear the mask”, the story now focuses on: “how to wear the mask”.
The idea of the multiverse itself gives so many possibilities for exploring free will.

<p class="note">For instance, “Everything Everywhere All At Once” - an absolute masterpiece - dives deep into the absurdistic idea of how everything is meaningless (because of the multiverse) but you can accept this and live quite freely.
<span class="more-info">Btw. Bagels and 42 are the meaning</span></p>

In the sci-fi multiverse/time-travel novel [“This is how you lose the time war”](https://www.goodreads.com/book/show/43352954-this-is-how-you-lose-the-time-war) by Amal El-Mohtar and Max Gladstone one book persists over all the different timelines: [“Travel Light”](https://www.goodreads.com/book/show/827276.Travel_Light?from_search=true&from_srp=true&qid=la0jK1uOXU&rank=2) by Naomi Mitchison.

<div class="tldr"><strong>TL;DR</strong> The concept of free will and reality in “Futurama”, “Spider-Verse” or “Everything Everywhere All At Once” offer a fresh perspective on free will</div>

### In a Nutshell

Breaking the 5th wall is more than a storytelling gimmick; it's a deep exploration of existential horror and free will. These themes resonate with the audience. Though harder to achieve than a 4th wall break, the 5th wall can be one of the best storytelling devices out there.

---

- [Tale Foundry’s Video on the 5th Wall](https://yewtu.be/watch?v=YrCfwLkspLY)

<div class="spoiler-banner" id="spoilerBanner">
    Spoiler Warning for Futurama and Doki Doki Literature Club.
    <span class="close-banner" tabindex="0" aria-label="Close" id="closeBannerButton">&times;</span>
</div>
<script src="/assets/js/warning-banner.js" defer></script>
