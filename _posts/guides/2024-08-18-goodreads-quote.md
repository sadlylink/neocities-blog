---
layout: post
title: Create Your Own Goodreads Quotes Widget (with open-source code)
date: 2024-08-19
tags: tutorial programming
categories: blog
keywords: Goodreads quotes, quotes widget, Python, JavaScript, CSV to JSON conversion, goodreads, quotes, open-source, free, no-tracking, privacy, no amazon
description: Learn how to create your own Goodreads quotes widget using Python and JavaScript. Convert CSV to JSON and display random quotes on your website. All open-source, no tracking, no amazon.
excerpt_separator: <!--more-->
last-edited: 2024-08-20
language: en
permalink: /blog/quote-widget.html
comments: false
---

If you're a fan of quotes and use Goodreads you might want to use their quotes-widget. The problem is Goodreads wants you to use their proprietary code for this. But there is an easy workaround for that.<a data-proofer-ignore></a> In this guide, I'll walk you through the process of exporting your quotes from Goodreads, converting them to JSON using Python, and displaying them on your website with JavaScript.<!--more-->

And all that open-source, with no tracking and without relying on amazon.

## Step 1: Download Your Quotes from Goodreads

To get started, download your quotes from Goodreads by visiting <https://goodreads.com/quotes/list>. Click on the "[Export My Quotes](https://www.goodreads.com/quotes/goodreads_quotes_export.csv)" button located at the top right corner of the page.

![Screenshot of the Goodreads interface with a red rectangle around "export my quotes"](/assets/images/blog/goodreads-quotes.jpg)

This will give you a CSV file containing your liked quotes.

## Step 2: Convert CSV to JSON with Python

Next, you'll need to convert the downloaded CSV file into a JSON format that can be easily used in your JavaScript code. Below is a Python script that accomplishes this:

<button class="copy-btn" data-target="python-code">Copy</button>
<div style="display: none" id="python-code">
import csv
import json

# Define the input and output file paths
input_csv = 'goodreads_quotes_export.csv'
output_json = 'quotes.json'

# Read the CSV file and extract the quote, author, and book
quotes = []
with open(input_csv, newline='', encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        quote = row['Quote']
        author = row['Author']
        book = row['Book']
        quotes.append({'quote': quote, 'author': author, 'book': book})

# Write the extracted data to a JSON file
with open(output_json, 'w', encoding='utf-8') as jsonfile:
    json.dump(quotes, jsonfile, ensure_ascii=False, indent=4)

print(f"Quotes have been successfully extracted to {output_json}")
</div>

```python
import csv
import json

# Define the input and output file paths
input_csv = 'goodreads_quotes_export.csv'
output_json = 'quotes.json'

# Read the CSV file and extract the quote, author, and book
quotes = []
with open(input_csv, newline='', encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        quote = row['Quote']
        author = row['Author']
        book = row['Book']
        quotes.append({'quote': quote, 'author': author, 'book': book})

# Write the extracted data to a JSON file
with open(output_json, 'w', encoding='utf-8') as jsonfile:
    json.dump(quotes, jsonfile, ensure_ascii=False, indent=4)

print(f"Quotes have been successfully extracted to {output_json}")
```

### Explanation of the Script

- The script reads the CSV file and extracts the quote, author, and book information.
- It then stores this data in a list of dictionaries and writes it to a JSON file.

## Step 3: Use the JSON in JavaScript

Now that you have your quotes in JSON format, you can use them on your website. The following JavaScript code fetches the JSON file and displays a random quote:

<button class="copy-btn" data-target="js-code">Copy</button>
<div style="display: none" id="js-code">
document.addEventListener("DOMContentLoaded", async function() {
    try {
        const response = await fetch('/quotes.json');
        const data = await response.json();
        const randomQuote = data[Math.floor(Math.random() * data.length)];
        const quoteElement = document.getElementById('quote_body');

        if (quoteElement) {
            quoteElement.innerHTML = `"<span title="${randomQuote.book}">${randomQuote.quote}</span>"&mdash; 
            <a href="https://en.wikipedia.org/w/index.php?search=${randomQuote.author}" title="Quote by" target="_blank">
            ${randomQuote.author}
            </a>`;
        } else {
            console.error('Quote element not found');
        }
    } catch (error) {
        console.error('Error fetching quotes:', error);
    }
});
</div>

```javascript
document.addEventListener("DOMContentLoaded", async function() {
    try {
        const response = await fetch('/quotes.json');
        const data = await response.json();
        const randomQuote = data[Math.floor(Math.random() * data.length)];
        const quoteElement = document.getElementById('quote_body');

        if (quoteElement) {
            quoteElement.innerHTML = `"<span title="${randomQuote.book}">${randomQuote.quote}</span>"&mdash; 
            <a href="https://en.wikipedia.org/w/index.php?search=${randomQuote.author}" title="Quote by" target="_blank">
            ${randomQuote.author}
            </a>`;
        } else {
            console.error('Quote element not found');
        }
    } catch (error) {
        console.error('Error fetching quotes:', error);
    }
});
```

### Customizing the Quote Display

You can personalize the quote widget by modifying the `quoteElement.innerHTML` value. This allows you to change how the quotes are presented on your site.

## Step 4: Embed the JavaScript in Your HTML

To display the quotes on your webpage, you need to embed the JavaScript code. Create a `<div>` element with the ID you used in the JavaScript file:

<button class="copy-btn" data-target="html-code">Copy</button>
<div style="display: none" id="html-code">
<h2>My Quotes</h2>
<div id="quote_body">
    <!-- Quote will be inserted here by JavaScript -->
    <!-- Default value for users without JavaScript enabled: -->
    "Because I could not stop for Death – <br/>He kindly stopped for me 
    – <br/>The Carriage held but just Ourselves – <br/>And Immortality." &mdash; 
    <a href="https://en.wikipedia.org/w/index.php?search=Emily_Dickinson" title="Emily Dickinson" target="_blank">Emily Dickinson</a>
</div>
<script src="/assets/js/quotes.js"></script>
</div>

```html
<h2>My Quotes</h2>
<div id="quote_body">
    <!-- Quote will be inserted here by JavaScript -->
    <!-- Default value for users without JavaScript enabled: -->
    "Because I could not stop for Death – <br/>He kindly stopped for me 
    – <br/>The Carriage held but just Ourselves – <br/>And Immortality." &mdash; 
    <a href="https://en.wikipedia.org/w/index.php?search=Emily_Dickinson" title="Emily Dickinson" target="_blank">Emily Dickinson</a>
</div>
<script src="/assets/js/quotes.js"></script>
```

You can add a default value inside the `<div>` element, that is displayed if JavaScript is disabled or something goes wrong.

## Conclusion

By following these steps, you can create a custom quotes widget for your website using your favorite quotes from Goodreads without relying on their proprietary code.

### Additional Resources

These are the code-snippets and steps I did to make my quotes widget [on my about page](/about#my-quotes).

So if you're interested in exploring the code further, you can visit the [GitLab repository for this site](https://gitlab.com/sadlylink/neocities-blog). The Python code and CSV file are located in the `_data` folder, the JSON file under `/assets/`, and the JavaScript file under `/assets/js/quotes.js`.

Happy quoting!

<script src="/assets/js/code-button.js"></script>
