---
layout: post
title: C++ Tutorial - 2
date: 2023-12-20
tags: coding cpp
categories: blog cpp
keywords: coding, programming, c++, cpp, c, tutorial, 
description: C++ Tutorial about functions, vectors and efficient iteration using range based for loops
excerpt_separator: <!--more-->
image: /assets/images/blog/cpp_logo.png
alt: The C++ logo by the Standard C++ Foundation
credit: Logo <a href="https://github.com/isocpp/logos" target="_blank">by Jeremy Kratz</a> (<a href="https://isocpp.org/home/terms-of-use" target="_blank">ToU</a>)
last-edited: 2024-08-19
language: en
permalink: /blog/cpp-tutorial-2.html
comments: false
---

## Functions, Vectors & Efficient Iteration

### 5. Functions

Functions in C++<a data-proofer-ignore></a> are crucial for encapsulating reusable code blocks, enhancing both modularity and maintainability in your programs.
<!--more-->

#### Function Declaration

A function declaration specifies the function’s name, return type, and parameters. For example:

```cpp
int add(int a, int b);
```

#### Function Definition

The function definition contains the executable code of the function. Here’s a simple function that adds two integers:

```cpp
int add(int a, int b) { // Function definition
    return a + b;
}
```

#### Parameters and Return Types

Functions can have parameters (inputs) and a return type (output). The types of the parameters define what data the function expects, and the return type specifies what data the function returns.
Default Parameters

Default parameters allow functions to be called with fewer arguments than they are defined with. Here’s an example:

```cpp
int multiply(int a, int b = 2) {
    return a * b; // If b is not specified, it defaults to 2
}
```

#### Function Overloading

Function overloading allows you to have multiple functions with the same name but different parameter lists or types, enhancing readability and flexibility. I had a bit to much fun using this. While it can be helpfull, e.g. when you have the same functionality and just slightly different input, it might get a bit messy if you use it for different utils but name it the same (not that I haven’t done that).

```cpp
int multiply(int a, int b = 2) {
    return a * b;
}

// Overloaded with a different type
double multiply(double a, double b = 2) {
    return a * b;
}
```

### 6. Introduction to Vectors

Vectors in C++ are dynamic arrays capable of resizing themselves automatically when elements are added or removed. They provide more flexibility than traditional arrays. Though those also exist in C++.
Declaration and Initialization

Vectors can be declared and initialized in various ways:

```cpp
std::vector<int> numbers; // Empty vector declaration
std::vector<int> nums = {1, 2, 3, 4, 5}; // Initialized with values
```

<div class="tldr"><strong>good-to-know</strong> Vectors use more memory than arrays to manage their dynamic sizing? It's a small price to pay for the convenience and safety they offer.</div>

#### Manipulating Vectors

Vectors offer methods like push_back(), pop_back(), and size() to add elements, remove elements, and get the vector size:

```cpp
numbers.push_back(6); // Adds '6' to the end of the vector
numbers.pop_back(); // Removes the last element
```

Swap two elements:

```cpp
std::swap(nums[0], nums[4]); // Swapping first and last elements
```

Find the maximum element:

```cpp
int maxElement = *max_element(nums.begin(), nums.end()); // Who's the biggest?
```

<p>Reade <a href="https://en.cppreference.com/w/cpp/container/vector">here</a> for more infos about vectors.</p>

### 7. Range-Based For Loop with Vectors

Range-based for loops offer a simpler and more readable way to iterate over elements in containers like vectors.
Syntax

Here’s how you can use a range-based for loop with vectors:

```cpp
std::vector<int> nums = {1, 2, 3, 4, 5};
for (int element : nums) {
    std::cout << element << " "; // Prints each element of the vector
}
```

Pretty neat

<div class="tldr"><strong>good-to-know</strong> The range-based for loop was introduced in C++11. It's a great example of how C++ is evolving to make coding easier and more intuitive.
Still some design choices of modern C++ are viewed quite critically
</div>

- Consult the C++ Documentation for more details on Functions and Vectors.
- Challenge yourself with projects that apply these concepts, like building a simple calculator or a basic to-do list.

<div class="tldr">
    <strong>Previous Tutorial</strong> &lt;&lt;&lt;<a href="/blog/cpp-tutorial-1.html">Part #1</a>&lt;&lt;&lt; <br />
    <strong>Next Tutorial</strong> &gt;&gt;&gt;<a href="/blog/">Link will be here when the third tutorial is out</a>&gt;&gt;&gt;
</div>
