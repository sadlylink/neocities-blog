import csv
import json
import os

# Get the directory of the current script
script_dir = os.path.dirname(os.path.abspath(__file__))

# Define the input and output file paths relative to the script's directory
input_csv = os.path.join(script_dir, 'goodreads_quotes_export.csv')
output_json = os.path.join(script_dir, '../assets/quotes.json')

# Read the CSV file and extract the quote and author
quotes = []
with open(input_csv, newline='', encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        quote = row['Quote']
        author = row['Author']
        book = row['Book']
        quotes.append({'quote': quote, 'author': author, 'book': book})

# Write the extracted data to a JSON file
with open(output_json, 'w', encoding='utf-8') as jsonfile:
    json.dump(quotes, jsonfile, ensure_ascii=False, indent=4)

print(f"Quotes have been successfully extracted to {output_json}")